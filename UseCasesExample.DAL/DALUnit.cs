﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.Common.Interfaces;
using UseCasesExample.DAL.Context;
using UseCasesExample.DAL.Enitities;

namespace UseCasesExample.DAL
{
	public class DALUnit : IDALUnit
	{
		UseCasesExampleContext _context;
		ILogger _logger;

		public DALUnit(UseCasesExampleContext context, ILogger logger)
		{
			_context = context;
		}

		public bool SaveChanges()
		{
			try
			{
				_context.SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return false;
			}
		}
	}
}
