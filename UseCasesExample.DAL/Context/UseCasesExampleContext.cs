﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.DAL.Enitities;

namespace UseCasesExample.DAL.Context
{
	public class UseCasesExampleContext : DbContext
	{
		public string Message { get; set; }

		public DbSet<UserEntity> UserSet { get; set; }

		public UseCasesExampleContext() : base("UseCasesExampleDB")
		{

		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			Database.SetInitializer(new DropCreateDatabaseAlways<UseCasesExampleContext>());
			base.OnModelCreating(modelBuilder);
		}
	}
}
