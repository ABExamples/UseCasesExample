﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.Common.DTO;
using UseCasesExample.Common.Interfaces;
using UseCasesExample.DAL.Context;
using UseCasesExample.DAL.Enitities;

namespace UseCasesExample.DAL.Repositories
{
	public class UserRepository : IRepository<UserEntity>
	{
		UseCasesExampleContext _context;
		ILogger _logger;

		public UserRepository(UseCasesExampleContext context, ILogger logger)
		{
			_context = context;
			_logger = logger;
		}

		public bool Create(UserEntity item)
		{
			try
			{
				_context.UserSet.Add(item);
				return true;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return false;
			}
		}

		public UserEntity Read(int id)
		{
			try
			{
				var result = _context.UserSet.Find(id);
				if (result == null)
				{
					_context.Message = $"User ID: {id} not found.";
				}
				return result;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return null;
			}
		}

		public IEnumerable<UserEntity> Read(Expression<Func<UserEntity, bool>> query)
		{
			try
			{
				var result = _context.UserSet.Where(query);
				if (result == null || result.Count() == 0)
				{
					_logger.Log($"Users with query \"{query}\" not found.");
				}
				return result;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return null;
			}
		}

		public bool Update(UserEntity item)
		{
			var dbItem = Read(item.ID);
			if (dbItem == null)
			{
				return false;
			}

			try
			{
				_context.Entry(dbItem).CurrentValues.SetValues(item);
				_context.Entry(dbItem).State = EntityState.Modified;
				return true;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return false;
			}
		}

		public bool Delete(UserEntity item)
		{
			var dbItem = Read(item.ID);
			if (dbItem == null)
			{
				return false;
			}

			try
			{
				_context.UserSet.Remove(dbItem);
				return true;
			}
			catch (Exception ex)
			{
				_logger.Log(ex);
				return false;
			}
		}
	}
}
