﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCasesExample.Common.UseCases.User
{
	public interface IUserCreateUseCase
	{
		bool Execute(string login, string password);
	}
}
