﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.Common.Interfaces;

namespace UseCasesExample.Common.Logger
{
	public class ConsoleLogger : ILogger
	{
		private string _lastItem;
		public string LastItem => _lastItem;

		public void Log(string Message)
		{
			_lastItem = Message;
			Debug.WriteLine(_lastItem);
		}

		public void Log(Exception ex)
		{
			var sb = new StringBuilder();
			sb.Append("Exception:\n");
			sb.Append(ex.Message);

			if(ex.InnerException != null)
			{
				sb.Append("\nInner Exception:\n");
				sb.Append(ex.InnerException.Message);
			}
			_lastItem = sb.ToString();
			Debug.WriteLine(_lastItem);
		}
	}
}
