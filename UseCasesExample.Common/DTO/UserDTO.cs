﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCasesExample.Common.DTO
{
	public class UserDTO
	{
		public int ID { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public DateTime DateCreated { get; set; }
	}
}
