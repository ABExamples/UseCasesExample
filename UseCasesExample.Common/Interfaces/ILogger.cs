﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCasesExample.Common.Interfaces
{
	public interface ILogger
	{
		string LastItem { get; }
		void Log(string Message);
		void Log(Exception ex);
	}
}
