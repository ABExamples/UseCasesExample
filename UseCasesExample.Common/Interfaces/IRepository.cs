﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UseCasesExample.Common.Interfaces
{
	public interface IRepository<T>
	{
		bool Create(T item);
		T Read(int id);
		IEnumerable<T> Read(Expression<Func<T ,bool>> query);
		bool Update(T item);
		bool Delete(T item);
	}
}
