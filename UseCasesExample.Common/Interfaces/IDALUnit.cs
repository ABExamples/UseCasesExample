﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCasesExample.Common.Interfaces
{
	public interface IDALUnit
	{
		bool SaveChanges();
	}
}
