﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.Common.Interfaces;
using UseCasesExample.Common.UseCases.User;
using UseCasesExample.DAL.Enitities;

namespace UseCasesExample.BAL.UseCases.User
{
	public class UserChangePasswordUseCase : IUserChangePasswordUseCase
	{
		ILogger _logger;
		IRepository<UserEntity> _userRepository;

		public UserChangePasswordUseCase(ILogger logger, IRepository<UserEntity> userRepository)
		{
			_logger = logger;
			_userRepository = userRepository;
		}

		public bool Execute(int userID, string oldPassword, string newPassword)
		{
			var user = _userRepository.Read(userID);
			if(user == null)
			{
				_logger.Log("Cant change password for non exsist user");
				return false;
			}

			if(user.Password != oldPassword)
			{
				_logger.Log("Not valid password for current user");
				return false;
			}

			user.Password = newPassword;

			var result = _userRepository.Update(user);

			return result;
		}
	}
}
