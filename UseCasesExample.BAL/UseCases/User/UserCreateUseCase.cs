﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCasesExample.Common.Interfaces;
using UseCasesExample.Common.UseCases.User;
using UseCasesExample.DAL.Enitities;

namespace UseCasesExample.BAL.UseCases.User
{
	public class UserCreateUseCase : IUserCreateUseCase
	{
		ILogger _logger;
		IRepository<UserEntity> _userRepository;

		public UserCreateUseCase(ILogger logger, IRepository<UserEntity> userRepository)
		{
			_logger = logger;
			_userRepository = userRepository;
		}

		public bool Execute(string login, string password)
		{
			var user = new UserEntity { Login = login, Password = password, DateCreated = DateTime.Now };
			var result = _userRepository.Create(user);

			return result;
		}
	}
}
