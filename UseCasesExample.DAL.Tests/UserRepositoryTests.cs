﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UseCasesExample.Common.Interfaces;
using UseCasesExample.Common.Logger;
using UseCasesExample.DAL.Context;
using UseCasesExample.DAL.Enitities;
using UseCasesExample.DAL.Repositories;

namespace UseCasesExample.DAL.Tests
{
	[TestClass]
	public class UserRepositoryTests
	{
		ILogger _logger;
		UseCasesExampleContext _context;
		IRepository<UserEntity> _userRepository;
		IDALUnit _dalUnit;

		UserEntity user1;
		UserEntity user2;

		[TestInitialize]
		public void Initialize()
		{
			_logger = new ConsoleLogger();
			_context = new UseCasesExampleContext();
			_userRepository = new UserRepository(_context, _logger);
			_dalUnit = new DALUnit(_context, _logger);

			user1 = new UserEntity { Login = "alice", Password = "123qwe", DateCreated = DateTime.Now };
			user2 = new UserEntity { Login = "bob", Password = "456asd", DateCreated = DateTime.Now };
		}

		[TestMethod]
		public void Create()
		{
			var result = _userRepository.Create(user1);

			if (result == true)
			{
				result = _dalUnit.SaveChanges();
			}

			Assert.IsTrue(result);
		}

		[TestMethod]
		public void Read()
		{
			Create();
			var result = _userRepository.Read(x => x.ID > 0);
			Assert.IsTrue(result.ToList().Count() > 0);
		}

		[TestMethod]
		public void Update()
		{
			Create();
			var resultList = _userRepository.Read(x => x.Login == user1.Login);

			Assert.IsNotNull(resultList);
			Assert.IsTrue(resultList.ToList().Count > 0);

			var oldItem = resultList.ToList().First();

			var newItem = new UserEntity
			{
				ID = oldItem.ID,
				Login = oldItem.Login,
				Password = "newpassword",
				DateCreated = oldItem.DateCreated
			};

			var result = _userRepository.Update(newItem);

			Assert.IsTrue(result);
		}

		[TestMethod]
		public void Delete()
		{
			Create();
			var resultList = _userRepository.Read(x => x.Login == user1.Login);

			Assert.IsNotNull(resultList);
			Assert.IsTrue(resultList.ToList().Count > 0);

			var oldItem = resultList.ToList().First();

			var result = _userRepository.Update(oldItem);

			Assert.IsTrue(result);
		}
	}
}
